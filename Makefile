OPT = g++ -O3 -std=c++11 -Wall -pedantic -Wextra -Werror

program: Makefile main.o MyVector.o
	$(OPT) main.o MyVector.o -o  program

main.o: Makefile main.cpp MyVector.h
	$(OPT) -c main.cpp

MyVector.o: Makefile MyVector.cpp MyVector.h
	$(OPT) -c MyVector.cpp

clean:
	rm *.o
