#include <vector>
#include <initializer_list>


class MyVector : public std::vector<int>
{
	using Collection = std::vector<int>;
public:	
	class MyIter; // custom iterator
	
	MyVector(std::initializer_list<int> l)
		: Collection(l) 
	{}

	MyIter myBegin();	
	MyIter myEnd();
};


class MyVector::MyIter
{
	friend class MyVector;
	using Collection = typename MyVector::Collection;
public:	
	int operator*() { return *iterator; }	
	bool operator==(const MyIter &other) const { return other.iterator==iterator; }
	bool operator!=(const MyIter &other) const { return other.iterator!=iterator; }
	MyIter& operator++();
	
private:
	Collection::iterator iterator;
	MyVector *vector;
		
	MyIter(const Collection::iterator &i, MyVector *v)
		: iterator(i), vector(v)
	{}
};
