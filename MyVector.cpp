#include "MyVector.h"	
#include <stdexcept>


MyVector::MyIter MyVector::myBegin()
{
	Collection::iterator it_min = Collection::begin();
	if(it_min==Collection::end())
		return MyIter(it_min, this);
	for(auto it = it_min+1; it!=Collection::end(); ++it)
	{
		if(*it<*it_min) // look for smallest element
			it_min = it;
	}
	return MyIter(it_min, this);
}


MyVector::MyIter MyVector::myEnd()
{
	return MyIter(Collection::end(), this);
}


MyVector::MyIter& MyVector::MyIter::operator++()
{
	if(iterator==vector->end())
		throw std::out_of_range("exceeding range in MyVector::MyIter::operator++()");
	const Collection::iterator start = this->iterator;
	for(++iterator; iterator!=vector->end(); ++iterator) // check elements: (*this, end) for equality
	{
		if(*iterator==*start) // element different from start, but with the same number
			return *this;
	}
	auto min = start;
	for(iterator=vector->begin(); iterator!=start; ++iterator) // check elements: [begin, *this)
	{
		// do not check if *iterator==*start, because iteration would loop infinitely when there are 2 equal elements in vector
		if(*iterator>*start)
		{
			if(min==start || *iterator<*min) // found new minimal element (greater than start)
				min = iterator;
		}
	}
	for(++iterator; iterator!=vector->end(); ++iterator)
	{
		if(*iterator>*start)
		{
			if(min==start || *iterator<*min) // found new minimal element (greater than start)
				min = iterator;
		}
	}
	if(min==start) // nothing has been found
		iterator = vector->end();
	else
		iterator = min;
	return *this;
}
