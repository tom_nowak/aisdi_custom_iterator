#include <cstdio>
#include "MyVector.h"

	
int main()
{
	MyVector vector = { 1, -1, 0, 1, 0, 2, 1, -1 };
	printf("Standard iterator:\n");	
	for(auto it = vector.begin(); it!=vector.end(); ++it)
	{	
		printf("%i ", *it);
	}
	printf("\nCustom iterator:\n");
	for(auto it = vector.myBegin(); it!=vector.myEnd(); ++it)
	{	
		printf("%i ", *it);
	}
	putchar('\n');
	return 0;
}
